import {
  CNFLogicConfig,
  CNFFunctionConfig,
  CNFConditionalConfig,
  ExtractorConfig,
  TokenMapConfig
} from './types'

export const cnfLogicConfig: CNFLogicConfig = {
  and: {
    display: '∧'
  },
  or: {
    display: '∨'
  }
};

export const cnfFunctionConfig: CNFFunctionConfig = {
  identity: {
    display: 'x → x',
    function: (x) => x,
    types: ['number', 'string', 'array:number', 'undefined']
  },
  abs: {
    display: 'abs',
    function: (x) => Math.abs(x),
    types: ['number']
  },
  mean: {
    display: 'mean',
    function: (nums) => (nums.reduce((add, val) => add + val) / nums.length),
    types: ['array:number']
  },
  max: {
    display: 'max',
    function: (nums) => Math.max(...nums),
    types: ['array:number']
  },
  min: {
    display: 'min',
    function: (nums) => Math.min(...nums),
    types: ['array:number']
  },
  length: {
    display: 'len',
    function: (arr) => arr.length,
    types: ['array', 'array:number', 'array:string', 'array:object']
  }
};

export const cnfConditionalConfig: CNFConditionalConfig = {
  eq: {
    display: '=',
    conditional: (a, b) => {
      // intentional == rather than === to allow for 1 == '1' to be true
      return a == b;
    },
    types: [
      'array',
      'array:string',
      'array:object',
      'array:number',
      'number',
      'string',
      'undefined'
    ]
  },
  neq: {
    display: '≠',
    types: [
      'array',
      'array:string',
      'array:object',
      'array:number',
      'number',
      'string',
      'undefined'
    ],
    conditional: (a, b) => a != b,
  },
  lt: {
    display: '<',
    types: ['number'],
    conditional: (a, b) => a < b,
  },
  gt: {
    display: '>',
    types: ['number'],
    conditional: (a, b) => a > b,
  },
  lte: {
    display: '≤',
    types: ['number'],
    conditional: (a, b) => a <= b,
  },
  gte: {
    display: '≥',
    types: ['number'],
    conditional: (a, b) => a >= b,
  },
  ss: {
    display: '⊂',
    types: ['array', 'array:string', 'array:number', 'string'],
    conditional: (a, b) => {
      let includes = false
      if (Array.isArray(a)) {
        for (let i = 0; i < a.length; i++) {
          if (a[i] == b) includes = true
          if (includes) return includes
        }
      } else if (typeof a === 'string') {
        return a.includes(b)
      } else { return includes }
      return includes
    }
  },
  nss: {
    display: '⟈',
    types: ['array', 'array:string', 'array:number', 'string'],
    conditional: (a, b) => {
      let includes = true
      if (Array.isArray(a)) {
        return a.every(e => e != b)
      } else if (typeof a === 'string') {
        return !a.includes(b)
      }
      return includes
    }
  }
};



export const tknFunctionMap: TokenMapConfig = {
  max: 'max',
  min: 'min',
  maximum: 'max',
  minimum: 'min',
  length: 'length',
  len: 'length',
  mean: 'mean',
  median: 'median',
  average: 'mean',
  ave: 'mean',
  abs: 'abs',
  absoulte: 'abs',
  module: 'abs',
  identity: 'identity'
}

export const tknConditionalMap: TokenMapConfig = {
  eq: 'eq',
  is: 'eq',
  equal: 'eq',
  '=': 'eq',
  '!=': 'neq',
  '≠': 'neq',
  gt: 'gt',
  'greater than': 'gt',
  '>': 'gt',
  '≥': 'gte',
  '>=': 'gte',
  'gte': 'gte',
  'greater than or equal to': 'gte',
  'less than': 'lt',
  lt: 'lt',
  '<': 'lt',
  '≤': 'lte',
  '<=': 'lte',
  'lte': 'lte',
  'less than or equal to': 'lte',
  'is not': 'neq',
  neq: 'neq',
  'not equal to': 'neq',
  'member of': 'ss',
  substring: 'ss',
  contains: 'ss',
  includes: 'ss',
  has: 'ss',
  'does not contain': 'nss',
  'does not include': 'nss',
  'does not have': 'nss',
  'not contain': 'nss',
  'not include': 'nss',
  'not have': 'nss',
  '!contains': 'nss',
  '!includes': 'nss',
  '!has': 'nss',
}

export const fieldRenderFunctions: ExtractorConfig = {}
export const fieldFilterFunctions: ExtractorConfig = {}
export const fieldSortByFunctions: ExtractorConfig = {}
