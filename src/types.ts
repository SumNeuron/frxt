// JSON SQL-eqsue types
export type Field = string
export type Fields = Field[]
export type RecordId = string
export type RecordIds = RecordId[]
export interface Record {
  [propName: string]: any;
}
export interface JSONSQL {
  [propName: string]: Record;
}

// Extractor Functions
export type ExtractorFunction = (id:string|number, field:string, record:object) => any
export interface ExtractorConfig {
  [propName: string]: ExtractorFunction;
}


// TimSort Types
export type SortFunctionDef = (a:any, b:any, isAscending:boolean) => boolean | number
export interface SortSpecification {
  field: string,
  isAscending: boolean
}
export type SortSpecifications = SortSpecification[]


// Conjunctive Normal Form Types
export type CNFFunctionDef = (x:any) => any
export type CNFConditionalDef = (a:any, b:any) => any

// |-- logic
export interface CNFLogicKeyConfig {
  display: string
}
export interface CNFLogicConfig {
  [propName: string]: CNFLogicKeyConfig;
}

// |-- function
export interface CNFFunctionKeyConfig {
  display: string,
  function: CNFFunctionDef,
  types: string[],
  exceptions?: {
    [propName: string]: CNFFunctionDef
  }
}
export interface CNFFunctionConfig {
  [propName: string]: CNFFunctionKeyConfig;
}

// |--conditional
export interface CNFConditionalKeyConfig {
  display: string,
  conditional: CNFConditionalDef,
  types: string[],
  exceptions?: {
    [propName: string]: CNFFunctionDef
  }
}
export interface CNFConditionalConfig {
  [propName: string]: CNFConditionalKeyConfig;
}


// Free Text Filter
export interface TokenMapConfig {
  [propName: string]: string;
}

export type GenericConfig = CNFLogicConfig|CNFFunctionConfig|CNFConditionalConfig|TokenMapConfig|ExtractorConfig

export type Logic = 'and' | 'or';
export interface Filter {
  logic: Logic,
  function: string,
  field: string,
  conditional: string,
  input: string
}
export type Filters = Filter[]

export interface FilterFromTextParams {
  input: string,
  tknFieldMap: TokenMapConfig,
  tknFunctionMap: TokenMapConfig,
  tknConditionalMap: TokenMapConfig,
  fieldTokens: string[]|undefined,
  functionTokens: string[]|undefined,
  conditionalTokens: string[]|undefined,
  scrubInputPattern: string|RegExp,
  debug: boolean,
}
export interface FiltersFromTextParams extends FilterFromTextParams{
  cnfLogicConfig:object,
  cnfFunctionsConfig:object,
  cnfConditionalConfig:object,
}
export type FilterFromTextFnDef = (FilterFromTextParams) => Filter
export type FiltersFromTextFnDef = (FiltersFromTextParams) => [object[], object[]]

// User Config Types
export interface FunctionLookup {
  [propName: string]: Function
}
export interface ExtractorProfileParams {
  [propName: string]: any
}
export interface ProfileDescription {
  functionName: string,
  params: ExtractorProfileParams
}
export interface ExtractorProfile {
  renderConfig: ProfileDescription,
  filterConfig: ProfileDescription,
  sortByConfig: ProfileDescription,
}
export interface ExtractorProfiles {
  [propName: string]: ExtractorProfiles
}
export interface MakeExtractorConfig {
  [propName: string]: ExtractorProfiles
}

export interface ExtractedFilter {
  filter: Filter,
  text: string
}
export type ExtractedFilters = ExtractedFilter[]
