import {JSONSQL, Field, Fields} from './types'
/**
 * @function jsonFields
 * Gives all fields for all records in SQL-like object
 * @param {JSONSQL} json - an object of {id: record} pairs
 * @returns {Fields} all fields inside any record of the provided json data.
 */
export const jsonFields =
function(json:JSONSQL): Fields {
  let all = [].concat(
    ...Object.keys(json)
    .map((key) => Object.keys(json[key]))
  );
  let unique = [...new Set(all)].sort();
  return unique;
}

/**
 * @function jsonFieldSpy
 * Looks at the first value of the field and attempts to determine the type
 * @param {JSONSQL} json - an object of {id: record} pairs
 * @param {Field} field - a field which could be found in record, e.g. record[field]
 * @returns {string} the extended type of the field (e.g. 'array:number')
 */
export const jsonFieldSpy =
function(json:JSONSQL, field:Field) {
  let first = Object.keys(json)[0];
  let value = json[first][field];
  return extendedType(value);
}

/**
 * @function jsonFieldTypes
 * Given an SQL-like object and a list of fields, returns the types for the values of those fields
 * @param {JSONSQL} json - an object of {id: record} pairs
 * @param {Fields} fields - an array of fields which can be found in any given record, e.g. record[fields[i]]
 * @returns {object} a mapping of the {fields: extendedType}
 */
export const jsonFieldTypes =
function(json:JSONSQL, fields:Fields) {
  let types = {} as object;
  fields.forEach(field => {
    types[field] = { type: jsonFieldSpy(json, field) };
  });
  return types;
}


/**
 * @function reduceJSON
 * Returns only the provided keys from the json
 * @param {JSONSQL} json - an object of {id: record} pairs
 * @param {string[]} keys - an array of keys of which to keep
 * @returns {object} the reduced json containing only the provided keys
 */
export const reduceJSON =
function(json:JSONSQL, keys:string[]) {
  return Object.keys(json)
    .filter(key => keys.includes(key))
    .reduce((obj, key) => {
      obj[key] = json[key];
      return obj;
    }, {});
}

/**
 * @function extendedType
 * A slight extension of the vanilla JS types
 * @param variable - any js variable
 * @returns {string} the extended type (e.g. 'array:number')
 */
export const extendedType =
function(variable) {
  // start with JS basics
  let type: string = typeof variable;

  /*
  number and string are sufficient to know which functions and conditionals
  can be applied to them.
  */
  if (["number", "string"].includes(type)) {
    return type;
  }

  /*
  if not a number or string, it might be an array.
  What the elements are of that array might modify what functions
  can be applied. E.g. an array of numbers allows for min, max, mean,
  etc to be applied. Whereas an array of strings does not.
  */
  if (type != "object") {
    return type;
  }

  /* only dealing with object types at this point */
  if (Array.isArray(variable)) {
    type = "array";
    if (!variable.length) {
      return type;
    }

    let subtypes = variable.map((e) => typeof e);
    let subtype = subtypes[0];
    let allSameQ = subtypes.every(e => e === subtype);

    if (allSameQ) {
      type = `${type}:${subtype};`
    }
    return type;
  }
  return type;
}


export default {
  jsonFields,
  jsonFieldSpy,
  jsonFieldTypes,
  reduceJSON,
  extendedType
}
